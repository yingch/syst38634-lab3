package password;

import static org.junit.Assert.*;
import org.junit.Test;

public class PasswordValidatorTest {

	@Test 
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invaid case chars", PasswordValidator.hasValidCaseChars("Ying"));
	}
	
	@Test 
	public void testHasValidCaseException() {
		assertTrue("Invaid case chars", !PasswordValidator.hasValidCaseChars("1234"));
	}
	
	@Test 
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invaid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test 
	public void testHasValidCaseCharsBoundaryOut() {
		assertFalse("Invaid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
//	@Test 
//	public void testIsValidLengthRegular() { 
//		boolean isValid = PasswordValidator.isValidLength("HelloWord"); assertTrue("Invalid password length", isValid == true);
//		}
//
//	@Test 
//	public void testIsValidLengthTestException() { 
//		boolean isValid = PasswordValidator.isValidLength("Hello"); assertTrue("Invalid password length", isValid == false); 
//		}
//	
//	@Test 
//	public void testIsValidLengthTestBoundaryIn() { 
//		boolean isValid = PasswordValidator.isValidLength("1234qwer"); assertTrue("Invalid password length", isValid == true); 
//		}
//	
//	@Test 
//	public void testIsValidLengthTestBoundaryOut() { 
//		boolean isValid = PasswordValidator.isValidLength("1234qwe"); assertTrue("Invalid password length", isValid == false);
//		}
	
//	@Test 
//	public void testHasTwoDigitsTestRegular() { 
//		boolean hadTwoDigits = PasswordValidator.hasTwoDigits("1234qwer"); assertTrue("Invalid password length", hadTwoDigits); 
//		}
//	
//	@Test 
//	public void testHasTwoDigitsTestException() { 
//		boolean hadTwoDigits = PasswordValidator.hasTwoDigits("HelloWord"); assertTrue("Invalid password length", hadTwoDigits == false); 
//		}
//
//	@Test 
//	public void testHasTwoDigitsTestBoundaryIn() { 
//		boolean hadTwoDigits = PasswordValidator.hasTwoDigits("12aaqwer"); assertTrue("Invalid password length", hadTwoDigits); 
//		}
//	
//	@Test 
//	public void testHasTwoDigitsTestBoundaryOut() { 
//		boolean hadTwoDigits = PasswordValidator.hasTwoDigits("1aaaqwer"); assertTrue("Invalid password length", hadTwoDigits == false); 
//	}
//	
	
}
