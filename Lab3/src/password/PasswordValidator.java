package password;

import java.util.regex.Pattern;

/**
 *
 * @author Ying Chen 991549628
 * Assume spaces are not valid characters for the purpose of calculating length.
 *
 */

public class PasswordValidator {

    public static boolean isValidLength(String password) {
        if (password == null || password.contains(" ")) {
            return false;
        }
        return password.length() >= 8;
    }

    public static boolean hasValidDigitCount(String password) {
        return Pattern.compile("^.*[0-9].*[0-9].*$").matcher(password).matches();
    }
    
    
    public static boolean hasTwoDigits(String password) {
    	return true;
    }
    
    public static boolean hasValidCaseChars(String password) {
    	return password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
    }
    
}
